let url = "http://localhost:3001/product";

export async function create(product) {
  return fetch(url, {
    method: "post",
    body: JSON.stringify(product),
  }).then((res) => res.json());
}

export async function findAll() {
  return fetch(url).then((res) => res.json());
}

export async function update(id, product) {
    return fetch(`${url}/${id}`, {
      method: "put",
      body: JSON.stringify(product),
    }).then((res) => res.json());
}

export async function remove(id) {
    return fetch(`${url}/${id}`, {method: "delete"}).then((res) => res.json());
}
