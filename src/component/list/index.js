import React from "react";
import "./index.css";

function List({ products, deleteProduct, setEditing, setCurrentProduct }) {
  return (
    <div className="product-list">
      <p>List products</p>
      <div className="product-list-header">
        <div className="product-list-header-column">Name</div>
        <div className="product-list-header-column">Quantity</div>
        <div className="product-list-header-column">Status</div>
        <div className="product-list-header-column"></div>
      </div>
      {products.map((product) => (
        <div className="product-list-row" key={product.id}>
          <div className="product-list-column">{product.name}</div>
          <div className="product-list-column">{product.quantity}</div>
          <div className="product-list-column">{product.status}</div>
          <div className="product-list-column">
            <button
              className="btn"
              onClick={() => {
                setEditing(true);
                setCurrentProduct(product);
              }}
            >
              <i className="fa fa-pencil"></i>
            </button>
            <button className="btn" onClick={() => deleteProduct(product.id)}>
              <i className="fa fa-trash"></i>
            </button>
          </div>
        </div>
      ))}
    </div>
  );
}

export default List;
